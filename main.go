package main

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func main() {
	connString := "postgres://postgres:p4ssw0rd@localhost:5432/postgres"
	dbPool, err := pgxpool.Connect(context.Background(), connString)
	if err != nil {
		println(err.Error())
		return
	}
	defer dbPool.Close()
	const N = 11
	var (
		wg   sync.WaitGroup
		rslt [N]int
		void string //OID 2278
	)
	wg.Add(N)
	start := time.Now()
	for i := 0; i < N; i++ {
		go func(i int) {
			defer wg.Done()
			err := dbPool.QueryRow(
				context.Background(),
				"select $1::int, pg_sleep(10)",
				i,
			).Scan(&rslt[i], &void)
			if err != nil {
				println(err.Error())
				return
			}
		}(i)
	}
	wg.Wait()
	sinceStart := time.Since(start)
	fmt.Println("hasil:", rslt, "\nlama semua query:", sinceStart)
}

//referensi: https://stackoverflow.com/a/35336850/19157961
